package com.servlets;

import adapters.StackExchangeAdapter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SearchingServlet extends HttpServlet {
    private final String QUERY_PARAMETER_NAME = "query";

    private final String PAGE_PARAMETER_NAME = "page";

    private final String RESULT_ATTRIBUTE_NAME = "result";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String query = request.getParameter(this.QUERY_PARAMETER_NAME);
        int page = 1;
        try
        {
            page = Integer.parseInt(request.getParameter(this.PAGE_PARAMETER_NAME));
        }
        catch (NumberFormatException ex) { }
        request.setAttribute(this.RESULT_ATTRIBUTE_NAME, StackExchangeAdapter.Search(query, page));
        RequestDispatcher dispatcher = request.getRequestDispatcher("/SearchResultView.jsp");
        dispatcher.forward(request, response);
    }
}
