package models;

import java.util.ArrayList;

public class SearchResultModel {

    private ArrayList<QuestionModel> questions;

    public ArrayList<QuestionModel> getQuestions() { return questions; }

    private String query;

    public String getQuery() { return query; }

    private int page;

    public int getPage() { return page; }

    private boolean hasMore;

    public boolean getHasMore() { return hasMore; }

    public SearchResultModel(String query, ArrayList<QuestionModel> questions, int page, boolean hasMore){
        this.query = query;
        this.questions = questions;
        this.page = page;
        this.hasMore = hasMore;
    }
}
