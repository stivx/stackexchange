package models;

import java.util.Date;

public class QuestionModel {
    private Date questionDate;

    public Date getQuestionDate() {
        return questionDate;
    }

    private String question;

    public String getQuestion() {
        return question;
    }

    private String author;

    public String getAuthor() {
        return author;
    }

    private String url;

    public String getUrl() {
        return url;
    }

    private long score;

    public long getScore() { return score; }

    private long answersCount;

    public long getAnswersCount() { return answersCount; }

    // TODO What is Answered? When do is_answered equals true or when answers_count is more 0 or when accepted_answer_id has value?
    private boolean isAnswered;

    public boolean getIsAnswered() {
        return isAnswered;
    }

    public QuestionModel(Date questionDate, String question, String author, String url, boolean isAnswered,
                         long answersCount, long score){
        this.questionDate = questionDate;
        this.question = question;
        this.author = author;
        this.url = url;
        this.isAnswered = isAnswered;
        this.answersCount = answersCount;
        this.score = score;
    }
}
