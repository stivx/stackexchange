package adapters;

import com.google.code.stackexchange.client.query.SearchApiQuery;
import com.google.code.stackexchange.client.query.StackExchangeApiQueryFactory;
import com.google.code.stackexchange.common.PagedList;
import com.google.code.stackexchange.schema.Paging;
import com.google.code.stackexchange.schema.Question;
import com.google.code.stackexchange.schema.StackExchangeSite;
import models.QuestionModel;
import models.SearchResultModel;

import java.util.ArrayList;

public class StackExchangeAdapter {

    private static final String APPLICATION_KEY = "qGuokmLSEaZgvRiY0DD4Ow((";

    private static final int DEFAULT_PAGE_SIZE = 20;

    // TODO Use stackexchange-java-sdk from maven, if pool request https://github.com/sanjivsingh/stackoverflow-java-sdk/pull/6 will be accepted.
    public static SearchResultModel Search(String query, int page){
        ArrayList<QuestionModel> resultQuestions = new ArrayList<>();
        if (query == null || query.isEmpty())
            return new SearchResultModel(query, resultQuestions, 0, false);
        final StackExchangeApiQueryFactory factory = StackExchangeApiQueryFactory
                .newInstance(APPLICATION_KEY, StackExchangeSite.STACK_OVERFLOW);
        final SearchApiQuery searchQuery = factory.newSearchApiQuery();
        PagedList<Question> questions = searchQuery
                .withInTitle(query)
                .withPaging(new Paging(page, DEFAULT_PAGE_SIZE))
                .list();
        for (Question q: questions) {
            resultQuestions.add(new QuestionModel(q.getCreationDate(), q.getTitle(), q.getOwner().getDisplayName(),
                    q.getLink(), q.getIsAnswered(), q.getAnswerCount(), q.getScore()));
        }
        return new SearchResultModel(query, resultQuestions, questions.getPage(), questions.hasMore());
    }
}
