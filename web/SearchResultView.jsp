<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 04/10/17
  Time: 20:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
    <title>Stack exchange searching</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
  </head>
  <body>
    <header>
      <h1>Search questions on Stack Exchange</h1>
    </header>
    <div id="content">
      <div id="queryArea">
        <form method="get">
          <input type="text" name="query" id="query" value="${result.getQuery()}" />
          <input type="submit" value="Search">
        </form>
      </div>
      <div id="searchResultArea">
        <table>
          <tr>
            <th>Date</th>
            <th>Score</th>
            <th>Question</th>
            <th>Author</th>
            <th>Answers</th>
          </tr>
          <c:forEach items="${result.getQuestions()}" var="row">
            <tr class="${row.getIsAnswered() ? "answered" : ""}">
              <td>${String.format("%1$td.%1$tm.%1$tY %1$tH:%1$tM:%1$tS", row.getQuestionDate())}</td>
              <td>${row.getScore()}</td>
              <td><a href="${row.getUrl()}">${row.getQuestion()}</a></td>
              <td>${row.getAuthor()}</td>
              <td>${row.getAnswersCount()}</td>
            </tr>
          </c:forEach>
        </table>
        <p>${String.format("Page %d", result.getPage())}</p>
        <div id="pageNavigation">
          <c:if test="${result.getPage() > 1}">
            <form method="get">
              <input type="text" name="query" value="${result.getQuery()}" hidden />
              <input type="text" name="page" value="${result.getPage() - 1}" hidden />
              <input type="submit" value="Previous" />
            </form>
          </c:if>
          <c:if test="${result.getHasMore()}">
            <form method="get">
              <input type="text" name="query" value="${result.getQuery()}" hidden />
              <input type="text" name="page" value="${result.getPage() + 1}" hidden />
              <input type="submit" value="Next" />
            </form>
          </c:if>
        </div>
      </div>
    </div>
  </body>
</html>
